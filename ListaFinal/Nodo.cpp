/*
 * Nodo.cpp
 *
 *  Created on: 27 de ago. de 2015
 *      Author: Marco
 */

#include "Nodo.h"

Nodo::Nodo() {
	// TODO Auto-generated constructor stub
	m_pSig=0;
	m_Dato=0;
}
Nodo::Nodo(int d) {
	m_Dato=d;
	m_pSig=0;

}
Nodo::Nodo(Nodo &N){
	m_Dato=N.m_Dato;
	m_pSig=N.m_pSig;

}
Nodo::~Nodo() {
	// TODO Auto-generated destructor stub
}

int Nodo::GetDato(){
	return m_Dato;
}
void Nodo::SetDato(int d){
	m_Dato=d;
}

void Nodo::SetSig(Nodo *E){
	m_pSig=E;
}
Nodo * Nodo::GetSig(){
	return m_pSig;
}
