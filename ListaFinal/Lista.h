/*
 * Lista.h
 *
 *  Created on: 27 de ago. de 2015
 *      Author: Marco
 */

#ifndef LISTA_H_
#define LISTA_H_
#include "Nodo.h"
using namespace std;

class Lista {
private:
	Nodo* m_pHead;//Cabeza de la lista
	Nodo* m_pLast;//Cola de la lista
	//bool listNew;
public:
	Lista();//Constructor por defecto
	~Lista();//Destructor
	void add(int);//Agregar
	void addFrom();
	void DeleteLast();//Eliminar el ultimo
	bool find(int d, Nodo **&p);//Buscar en la lista
	void Print();//Imprimir la lista

};

#endif /* LISTA_H_ */
