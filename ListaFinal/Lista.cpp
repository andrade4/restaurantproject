/*
 * Lista.cpp
 *
 *  Created on: 27 de ago. de 2015
 *      Author: Marco
 */

#include "Lista.h"
#include "Nodo.h"
#include <iostream>
using namespace std;

Lista::Lista() {
	// TODO Auto-generated constructor stub
	m_pHead=0;
	m_pLast=0;
}


Lista::~Lista() {
	// TODO Auto-generated destructor stub
}


bool Lista::find(int d, Nodo **&p)
{
    p = &m_pHead;
    while (*p) {
        if ((*p)->GetDato()== d)
            return true;
        if ((*p)->GetDato() > d)
            return false;
        p =&((*p)->m_pSig);
    }
    return false;
}


void Lista::add(int d)
{
    Nodo **q;
    if (this->find(d, q))
        return;
    Nodo *pNew = new Nodo(d);
    pNew->SetSig(*q);
    *q = pNew;
}


void Lista::Print(){


	Nodo *P = m_pHead;
	for( ;P;P=P->m_pSig)
		cout<<P->m_Dato<<"-";

}
