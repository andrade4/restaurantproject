/*
 * Nodo.h
 *
 *  Created on: 27 de ago. de 2015
 *      Author: Marco
 */

#ifndef NODO_H_
#define NODO_H_

class Nodo {
public:
	int m_Dato;
	Nodo *m_pSig;
public:
	Nodo();//Constructor por defecto
	Nodo(int);//Constructor con parametros
	Nodo(Nodo &N);

	~Nodo();//Destructor
	int GetDato();//Obtener dato del nodo
	void SetDato(int);//
	void SetSig(Nodo*);
	Nodo *GetSig();//Obtener el siguiente nodo

};

#endif /* NODO_H_ */
